#!/bin/sh

set -e

function finish {
  docker.logout
}
trap finish EXIT

GitRepo="https://github.com/evancohen/smart-mirror.git"

if [ "${CI_COMMIT_BRANCH}" = "master" ]; then
  echo "branch is master"
  BuildRef="master"
else
  echo "branch is not master"
  BuildRef="dev"
fi
echo "BuildRef="${BuildRef}

# use raspi image if arm
if [ "${imgarch}" = "arm" ]; then
  image_build="balenalib/raspberrypi3-node"
  image_run="balenalib/raspberrypi3-node"
else
  image_build="node:18-buster"
  image_run="node:18-buster-slim"
fi

echo "image_build="${image_build}
echo "image_run="${image_run}

docker.gitlab.login

build --context ./build \
  --dockerfile Dockerfile \
  --destination ${CI_REGISTRY_IMAGE}:${CI_COMMIT_BRANCH}_${imgarch} \
  --build-arg image_build=${image_build} \
  --build-arg image_run=${image_run} \
  --build-arg imgarch=${imgarch} \
  --build-arg BuildRef=${BuildRef} \
  --build-arg GitRepo=${GitRepo}
