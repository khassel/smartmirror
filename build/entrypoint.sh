#!/bin/sh

set -e

echo "starting smartmirror ..."

if [ "$(uname -m)" = "x86_64" ]; then
  # replace "default" in speech config
  # sed -i 's|"device": "default",|"device": "plughw:0,0",|' config.json
  echo "running on $(uname -m)"
else
  # set amixer audio output
  if [ "$AUDIO" = "3.5mm" ]; then
    amixer cset numid=3 1
    echo "Audio forced to 3.5mm jack."
  else
    amixer cset numid=3 2
    echo "Audio forced to HDMI."
  fi
fi

# fixes AutoTimer:
sed -i 's|chvt|sudo chvt|g;s|    must_be_root|    #must_be_root|g' scripts/raspi-monitor.sh
sed 's|"wakeCmd": "sudo ./scripts/raspi-monitor.sh on > /dev/null 2>&1",|"wakeCmd": "./scripts/raspi-monitor.sh on",|g' config.json > tmp.config.json && cat tmp.config.json > config.json
sed 's|"sleepCmd": "sudo ./scripts/raspi-monitor.sh off > /dev/null 2>&1"|"sleepCmd": "./scripts/raspi-monitor.sh off"|g' config.json > tmp.config.json && cat tmp.config.json > config.json

# fix motion.js:
sed -i 's|var Raspi = require("raspi-io");|var Raspi = require("raspi-io").RaspiIO;|g' motion.js

# set TZ variable
[ -z "$TZ" ] && export TZ="$(cat /etc/timezone)"

exec "$@"