# **Smart Mirror**

is a voice controlled life automation hub, most commonly powered by the Raspberry Pi. For more info visit the [project website](https://github.com/evancohen/smart-mirror). This project packs Smart Mirror into a docker image.

> 👉 This docker setup is very young, so don't expect everything is working already ...

# Why Docker?

Using docker simplifies the setup by using the container instead of setting up the host with installing all the node.js stuff etc.
Getting/Updating the container is done with one command.

# Docker Images

The docker image `karsten13/smartmirror` is provided in this versions:

TAG     | OS/ARCH     | DESCRIPTION
------- | ----------- | -----------------------------------------------------------
latest  | linux/arm   | for raspberry pi, based on debian buster
latest  | linux/amd64 | for any desktop linux, based on debian buster (I use this for testing due to performance reasons)

Smart Mirror has no releases or version numbers, so the above tags are floating tags and therefore overwritten with every new build.
The `master` branch of the project is used for the build.

# Installation prerequisites

* running raspberry pi version >2 with running raspian with LAN or WLAN access
* [Docker](https://docs.docker.com/engine/installation/)
* [docker-compose](https://docs.docker.com/compose/install/)

### Setup for graphical desktop
- install unclutter: `sudo apt-get install -y unclutter`
- edit (here with nano) `sudo nano /etc/xdg/lxsession/LXDE-pi/autostart` and insert the following lines for disabling screensaver and mouse cursor:
> Hint: With older debian versions you must edit this file instead `nano /home/pi/.config/lxsession/LXDE-pi/autostart`.

````bash
@xset s noblank
@xset s off
@xset -dpms
@unclutter -idle 0.1 -pi
@xhost +local:
````
	
- uncomment the existing lines, otherwise you will see the pi desktop before Smart Mirror has started
- edit (here with nano) ```nano ~/.bashrc``` and insert the following line (otherwise docker has no access on the pi display):
````bash
xhost +local:
````
- execute `sudo raspi-config` and navigate to "3 boot options" and choose "B2 Wait for Network at Boot".

> Before next installation steps please reboot your pi 

# Installation of this Repository

Open a shell in your home directory and run
````bash
git clone https://gitlab.com/khassel/smartmirror.git
````

# Start Smart Mirror

Navigate to `~/smartmirror/run` and execute

````bash
docker-compose up -d
````

The container will start and the Smart Mirror should appear on the screen of your pi.

> The container is configured to restart automatically so after executing `docker-compose up -d` it will restart with every reboot of your pi.


You can see the logs with

````bash
docker logs smart
````

Executing
````bash
docker ps -a
````
will show all containers and 

````bash
docker-compose down
````

will stop and remove the Smart Mirror container.

# Configuration

Please refer to the [project dokumentation](https://docs.smart-mirror.io/configuration/configure_the_mirror).

Contrary to the docs you start the mirror with `docker-compose up -d` and you can get the logs with `docker logs smart`.
You find the url for the remote configuration site in the logs or on the mirror display.

The `config.json` (where all the configuration stuff is stored) is saved on the host in `~/smartmirror/run`.
